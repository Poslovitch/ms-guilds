package fr.poslovitch.guilds;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class GuildChatListener implements Listener{
	
	Guilds main;

	public GuildChatListener(Guilds plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.main = plugin;
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		String msg = e.getMessage();
		
		if(msg.startsWith("*")){
			e.setCancelled(true);
			Guild g = Guilds.getGuild(p);
			
			for(UUID u : g.getMembers()){
				if(Bukkit.getPlayer(u).isOnline()) Bukkit.getPlayer(u).sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "Guilde" + ChatColor.GRAY + "] " + ChatColor.YELLOW + p.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg.substring(1));
			}
		}
	}
}
