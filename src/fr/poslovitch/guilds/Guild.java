package fr.poslovitch.guilds;

import java.util.ArrayList;
import java.util.UUID;

public class Guild {
	
	String name;
	UUID leader;
	ArrayList<UUID> members;
	
	public Guild(String name, UUID leader, ArrayList<UUID> members){
		this.name = name;
		this.leader = leader;
		this.members = members;
	}
	
	public String getName(){return name;}
	public UUID getLeader(){return leader;}
	public ArrayList<UUID> getMembers(){return members;}
	
}
