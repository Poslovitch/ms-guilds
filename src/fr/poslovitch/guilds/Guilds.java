package fr.poslovitch.guilds;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.poslovitch.guilds.commands.GuildCommand;

public class Guilds extends JavaPlugin{
	
	private static ArrayList<Guild> guilds = new ArrayList<>();
	
	public void onEnable(){
		new GuildChatListener(this);
		new GuildCommand(this);
		
		getLogger().info("Guilds enabled.");
	}
	
	public void onDisable(){
		getLogger().info("Guilds disabled.");
	}
	
	public static Guild getGuild(Player p){
		for(Guild g : guilds){
			if(g.getMembers().contains(p.getUniqueId())) return g;
		}
		return null;
	}
	
	public static void addGuild(Guild g){
		guilds.add(g);
	}
}
